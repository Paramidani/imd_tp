/******************************************************************************
 * Description: Test for MPU9250 driver
 * Author: Matias Paramidani
 * Date: 2021-11-30
 *******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include "imd_mpu9250.h"

#define MEAS_CYCLES 30

int main(){
    int i = 0;
    printf("INFO: Inicia la prueba del driver para el dispositivo MPU9250.\n");

    mpu9250Init( MPU9250_ADDRESS_0 );
    while(i < MEAS_CYCLES){
        mpu9250Read();

        printf( "Giróscopo:      (%f, %f, %f)   [rad/s]\r\n",
                mpu9250GetGyroX_rads(),
                mpu9250GetGyroY_rads(),
                mpu9250GetGyroZ_rads()
                );
        printf( "Acelerómetro:   (%f, %f, %f)   [m/s2]\r\n",
                mpu9250GetAccelX_mss(),
                mpu9250GetAccelY_mss(),
                mpu9250GetAccelZ_mss()
                );
		printf( "Magnetometro:   (%f, %f, %f)   [uT]\r\n",
                mpu9250GetMagX_uT(),
                mpu9250GetMagY_uT(),
                mpu9250GetMagZ_uT()
                );
        printf( "Temperatura:    %f   [C]\r\n\r\n",
                mpu9250GetTemperature_C()
                );
        usleep(1000000);
        i++;
    }
    printf("INFO: Fin del programa de prueba\n");
    return 0;
}
