#include <stdio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    int ret_wr = 0;
    char mode;
    char reg_address;
    char data;

    printf("\n1. Insert transfer mode (0-1):\n");
    scanf("%x", (char *)&mode);

    printf("\n2. Insert data to write (1 byte):\n");
    scanf("%x", (char *)&data);

    printf("\n3. Insert register address:\n");
    scanf("%x", (char *)&reg_address);

    int my_dev = open("/dev/mse00", O_RDWR);
    if (my_dev < 0) {
        perror("ERROR: Fail to open device file: /dev/mse00.\n");
    } else {
        printf("INFO: Device open succesfully.\n");

        // Configuro el modo de transferencia
        ioctl(my_dev, 1, (unsigned long) mode);

        // Configuro la direccion de registro con ioctl
        ioctl(my_dev, 0, reg_address);

        ret_wr = write(my_dev, &data, sizeof(data));
        if (ret_wr < 0) {
            printf("Failed to write the message to the device.\n");
            return errno;
        }
        printf("INFO: Closing device.\n");
        close(my_dev);
    }
    return 0;
}
