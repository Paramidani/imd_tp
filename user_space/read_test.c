#include <stdio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    int ret_wr = 0;
    int ret_rd = 0;
    char msg[2];
    char reg_address;
    char mode;

    printf("\n1. Insert transfer mode (0-1):\n");
    scanf("%x", (char *)&mode);

    printf("\n2. Insert register address:\n");
    scanf("%x", (char *)&reg_address);

    int my_dev = open("/dev/mse00", O_RDWR);
    if (my_dev < 0) {
        perror("ERROR: Fail to open device file: /dev/mse00.\n");
    } else {
        printf("INFO: Device open succesfully.\n");

        // Configuro el modo de transferencia
        ioctl(my_dev, 1, (unsigned long) mode);

        // Configuro la direccion de registro con ioctl
        ioctl(my_dev, 0, (unsigned long) reg_address);

        ret_rd = read(my_dev, &msg, sizeof(msg));
        if (ret_rd < 0) {
            printf("ERROR: Failed to read a message from the device.\n");
            return errno;
        }
        else {
            printf("INFO: Value read is: %x - %x\n", msg[1], msg[0]);
        }
        printf("INFO: Closing device.\n");
        close(my_dev);
    }
    return 0;
}
