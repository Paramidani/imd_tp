## Kernel driver

### Descripción

Para poder escribir el driver haciendo uso de las API del núcleo i2c, se utiliza, al igual que en el ejemplo de clase, una estructura privada, la cual contiene la estructura que caracteriza el dispositivo i2c y la estructura miscdevice.

```c
/* Private device structure */
struct mpu9250_dev {
	struct i2c_client *client;
	struct miscdevice mse_miscdevice;
	char name[9]; /* msedrvXX */
};
```

Dentro de la función mpu9250_probe se hace un devm_kzalloc() para generar dinámicamente una instancia de dicha estructura. A esta instancia se le asigna la estructura i2c_client() del dispositivo y se cargan todos los datos necesarios dentro de la estructura miscdevice (nombre del driver, número minor y la estructura de file-operations). La memoria ocupada por la instancia de mpu9250_dev() es liberada por la función mpu9250_remove() al momento de remover el módulo.

#### File opearation IOCTL

```c
static long mpu9250_ioctl(struct file *file, unsigned int cmd, unsigned long arg)  {

    struct mpu9250_dev *mse; /* Puntero a la estructura privada del device */

    /* pr_info("INFO: mpu9250_ioctl() fue invocada. cmd = %d, arg = %ld\n", cmd, arg); */

    mse = container_of(file->private_data, struct mpu9250_dev, mse_miscdevice);

    switch (cmd) {
        case 0: /* Set register address */
            reg_addr = (char) arg;
            break;
        case 1: /* Set transfer mode */
            if ((arg!=0) && (arg!=1)) {
                pr_err("ERROR: Error al ejecutar mpu9250_ioctl(). Mode should be 0x00 or 0x01, %d not valid option.\n", (char)arg);
                return -1;
            }
            else {
                xfer_mode = (char) arg;
            }
            break;
        default:
            pr_info("WARN: ioctl cmd option selected is not valid.\n");
            reg_addr = 0x00;
            xfer_mode = 0x00;
    }

    return 0;
}
```

#### File opearation WRITE

Hace uso de la API i2c_master_send():

```c
/* Funcion para escribir el dispositivo /dev/mseXX utilizando write() en espacio usuario */
static ssize_t mpu9250_write(struct file *file, const char __user *buffer, size_t len, loff_t *offset)  {

    struct mpu9250_dev *mse; /* Puntero a la estructura privada del device */
    int wr_stat;
    int error_count = 0;
    struct i2c_msg msg;

    /* pr_info("INFO: mpu9250_write() fue invocada.\n"); */

    /* A partir del puntero a file obtengo la estructura completa del device */
    mse = container_of(file->private_data, struct mpu9250_dev, mse_miscdevice);

    message[0] = reg_addr;

    error_count = copy_from_user(&message[1] ,buffer, len);
    if (error_count != 0) {
        pr_err("ERROR: Error al ejecutar copy_from_user(). Return value: %d\n", error_count);
        return -1;
    }

    if (xfer_mode == 0x01) {
        wr_stat = i2c_master_send(mse->client,message,(len+1));
        if (wr_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_send(). Return value: %d\n", wr_stat);
            return wr_stat;
        }
    }
    else {
        msg.addr = mse->client->addr; /* I2C Device address */
        msg.flags = 0; /* Mark as write data */
        msg.buf = message; /* Data buffer to send */
        msg.len = len+1; /* Length of data to send */
        wr_stat = i2c_transfer(mse->client->adapter, &msg, 1);
        if (wr_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_transfer(). Return value: %d\n", wr_stat);
            return wr_stat;
        }
    }

    return wr_stat;
}
```

#### File opearation READ

Hace uso de las APIs i2c_master_send() e i2c_master_recv():

```c
/* Funcion para leer el dispositivo /dev/mseXX utilizando read() en espacio usuario */
static ssize_t mpu9250_read(struct file *file, char __user *userbuf, size_t count, loff_t *ppos)  {

    struct mpu9250_dev *mse; /* Puntero a la estructura privada del device */
    int rd_stat;
    int wr_stat;
    int error_count = 0;
    struct i2c_msg msg[2];

    /* pr_info("INFO: mpu9250_read() fue invocada.\n"); */

    /* A partir del puntero a file obtengo la estructura completa del device */
    mse = container_of(file->private_data, struct mpu9250_dev, mse_miscdevice);

    if (xfer_mode == 0x01) {
        wr_stat = i2c_master_send(mse->client,&reg_addr,1);
        if (wr_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_send(). Return value: %d\n", wr_stat);
            return wr_stat;
        }
        rd_stat = i2c_master_recv(mse->client, message, count);
        if (rd_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_recv(). Return value: %d\n", rd_stat);
            return rd_stat;
        }
    }
    else {
        /* msg[0]，The first write message sends the first address of the register to be read */
        msg[0].addr = mse->client->addr; /* I2C Device address */
        msg[0].flags = 0; /* Mark as send data */
        msg[0].buf = &reg_addr; /* First address read */
        msg[0].len = 1; /* reg length */
        /* msg[1]，The second read message reads the register data */
        msg[1].addr = mse->client->addr; /* I2C Device address */
        msg[1].flags = I2C_M_RD; /* Mark as read data  */
        msg[1].buf = message; /* Read data buffer */
        msg[1].len = count; /* Length of data to read */

        rd_stat = i2c_transfer(mse->client->adapter, msg, 2);
        if(rd_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_recv(). Return value: %d\n", rd_stat);
            return rd_stat;
        }
    }

    error_count = copy_to_user(userbuf, message, count);
    if (error_count != 0) {
        pr_err("ERROR: Error al ejecutar copy_to_user(). Return value: %d\n", error_count);
        return -1;
    }

    return rd_stat;
}
```

### Compilación

1. Abrir una terminal y correr los siguientes comandos:

```
export PATH=$PATH:~/MSE/ISO_II/toolchain/arm-mse-linux-gnueabihf/bin
export CROSS_COMPILE=arm-linux-
export ARCH=arm
```

2. Ir hasta la carpeta donde se encuentra el driver (esta carpeta)

3. Ejecutar el comando "make".

4. El módulo a cargar es "mse_mpu9250_driver.c"

### Insert and remove

Para insertar el módulo:
1. Dentro de la consola de la SBC navegar hasta el directorio donde se encuentra el módulo compilado
2. Ejecutar: "insmod mse_mpu9250_driver.ko"
3. Debe observarse el mensaje de que el módulo fue cargado correctamente, el nombre del device y el número minor asignado.

Para remover el módulo:
1. Ejecutar: "rmmod mse_mpu9250_driver.ko"
2. Debe observarse un mensaje que anuncia que el módulo fue removido correctamente.

### Verificacion de carga
Se verificó que el driver se haya registrado a si mismo dentro del platform bus:
```
ls -l /sys/bus/i2c/drivers
```
![Check_1](../imagenes/check_1.jpg)

```
ls -l /sys/bus/i2c/devices
```
![Check_2](../imagenes/check_2.jpg)

```
ls /sys/bus/i2c/drivers/mpu9250_driver
```
![Check_3](../imagenes/check_3.jpg)

Por otro lado se verificó que el módulo fuese visible dentro de /sys y el dispositivo haya sido registrado dentro de la clase correspondiente al framework utilizado:
```
ls -l /sys/module/mse_mpu9250_driver/drivers
```
![Check_4](../imagenes/check_4.jpg)

```
ls /sys/class/misc
```
![Check_5](../imagenes/check_5.jpg)

Por último, se verificó que udev haya creado el file device en devtmpfs:
```
ls -l /dev | grep mse00
```
![Check_6](../imagenes/check_6.jpg)