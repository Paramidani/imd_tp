#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/i2c.h>
#include <linux/fs.h>
#include <linux/of.h>
#include <linux/uaccess.h>

/* Private device structure */
struct mpu9250_dev {
	struct i2c_client *client;
	struct miscdevice mse_miscdevice;
	char name[9]; /* msedrvXX */

};

/* Memory for the string that is passed from userspace */
static char message[256] = {0};
static char reg_addr = 0x00;
static char xfer_mode = 0x00; /*Mode 0x00 uses i2c_transfer(), mode 0x01 uses i2c_master_send/recv() */ 

/*
 * Definicion de los ID correspondientes al Device Tree. Estos deben ser informados al
 * kernel mediante la macro MODULE_DEVICE_TABLE
 *
 * NOTA: Esta seccion requiere que CONFIG_OF=y en el kernel
 */

static const struct of_device_id mpu9250_dt_ids[] = {
	{ .compatible = "mse,mpu9250", },
	{ /* sentinel */ }
};

MODULE_DEVICE_TABLE(of, mpu9250_dt_ids);

/* Funcion para leer el dispositivo /dev/mseXX utilizando read() en espacio usuario */
static ssize_t mpu9250_read(struct file *file, char __user *userbuf, size_t count, loff_t *ppos)  {

    struct mpu9250_dev *mse; /* Puntero a la estructura privada del device */
    int rd_stat;
    int wr_stat;
    int error_count = 0;
    struct i2c_msg msg[2];

    /* pr_info("INFO: mpu9250_read() fue invocada.\n"); */

    /* A partir del puntero a file obtengo la estructura completa del device */
    mse = container_of(file->private_data, struct mpu9250_dev, mse_miscdevice);

    if (xfer_mode == 0x01) {
        wr_stat = i2c_master_send(mse->client,&reg_addr,1);
        if (wr_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_send(). Return value: %d\n", wr_stat);
            return wr_stat;
        }
        rd_stat = i2c_master_recv(mse->client, message, count);
        if (rd_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_recv(). Return value: %d\n", rd_stat);
            return rd_stat;
        }
    }
    else {
        /* msg[0]，The first write message sends the first address of the register to be read */
        msg[0].addr = mse->client->addr; /* I2C Device address */
        msg[0].flags = 0; /* Mark as send data */
        msg[0].buf = &reg_addr; /* First address read */
        msg[0].len = 1; /* reg length */
        /* msg[1]，The second read message reads the register data */
        msg[1].addr = mse->client->addr; /* I2C Device address */
        msg[1].flags = I2C_M_RD; /* Mark as read data  */
        msg[1].buf = message; /* Read data buffer */
        msg[1].len = count; /* Length of data to read */

        rd_stat = i2c_transfer(mse->client->adapter, msg, 2);
        if(rd_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_recv(). Return value: %d\n", rd_stat);
            return rd_stat;
        }
    }

    error_count = copy_to_user(userbuf, message, count);
    if (error_count != 0) {
        pr_err("ERROR: Error al ejecutar copy_to_user(). Return value: %d\n", error_count);
        return -1;
    }

    return rd_stat;
}

/* Funcion para escribir el dispositivo /dev/mseXX utilizando write() en espacio usuario */
static ssize_t mpu9250_write(struct file *file, const char __user *buffer, size_t len, loff_t *offset)  {

    struct mpu9250_dev *mse; /* Puntero a la estructura privada del device */
    int wr_stat;
    int error_count = 0;
    struct i2c_msg msg;

    /* pr_info("INFO: mpu9250_write() fue invocada.\n"); */

    /* A partir del puntero a file obtengo la estructura completa del device */
    mse = container_of(file->private_data, struct mpu9250_dev, mse_miscdevice);

    message[0] = reg_addr;

    error_count = copy_from_user(&message[1] ,buffer, len);
    if (error_count != 0) {
        pr_err("ERROR: Error al ejecutar copy_from_user(). Return value: %d\n", error_count);
        return -1;
    }

    if (xfer_mode == 0x01) {
        wr_stat = i2c_master_send(mse->client,message,(len+1));
        if (wr_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_master_send(). Return value: %d\n", wr_stat);
            return wr_stat;
        }
    }
    else {
        msg.addr = mse->client->addr; /* I2C Device address */
        msg.flags = 0; /* Mark as write data */
        msg.buf = message; /* Data buffer to send */
        msg.len = len+1; /* Length of data to send */
        wr_stat = i2c_transfer(mse->client->adapter, &msg, 1);
        if (wr_stat < 0) {
            pr_err("ERROR: Error al ejecutar i2c_transfer(). Return value: %d\n", wr_stat);
            return wr_stat;
        }
    }

    return wr_stat;
}

static long mpu9250_ioctl(struct file *file, unsigned int cmd, unsigned long arg)  {

    struct mpu9250_dev *mse; /* Puntero a la estructura privada del device */

    /* pr_info("INFO: mpu9250_ioctl() fue invocada. cmd = %d, arg = %ld\n", cmd, arg); */

    mse = container_of(file->private_data, struct mpu9250_dev, mse_miscdevice);

    switch (cmd) {
        case 0: /* Set register address */
            reg_addr = (char) arg;
            break;
        case 1: /* Set transfer mode */
            if ((arg!=0) && (arg!=1)) {
                pr_err("ERROR: Error al ejecutar mpu9250_ioctl(). Mode should be 0x00 or 0x01, %d not valid option.\n", (char)arg);
                return -1;
            }
            else {
                xfer_mode = (char) arg;
            }
            break;
        default:
            pr_info("WARN: ioctl cmd option selected is not valid.\n");
            reg_addr = 0x00;
            xfer_mode = 0x00;
    }

    return 0;
}

/* declaracion de una estructura del tipo file_operations */

static const struct file_operations mpu9250_fops = {
	.owner = THIS_MODULE,
	.read = mpu9250_read,
	.write = mpu9250_write,
	.unlocked_ioctl = mpu9250_ioctl,
};

/*--------------------------------------------------------------------------------*/
static int mpu9250_probe(struct i2c_client *client, const struct i2c_device_id *id)  {

    struct mpu9250_dev * mse; /* Puntero a la estructura privada del device */
    static int counter = 0;
    int ret_val;

    /* Allocate new private structure */
    mse = devm_kzalloc(&client->dev, sizeof(struct mpu9250_dev), GFP_KERNEL);

    /* Store pointer to the device-structure in bus device context */
    i2c_set_clientdata(client,mse);

    /* Store pointer to I2C client device in the private structure */
    mse->client = client;

    /* Initialize the misc device, mse is incremented after each probe call */
    sprintf(mse->name, "mse%02d", counter++);

    mse->mse_miscdevice.name = mse->name;
    mse->mse_miscdevice.minor = MISC_DYNAMIC_MINOR;
    mse->mse_miscdevice.fops = &mpu9250_fops;

    /* Register misc device */
    ret_val = misc_register(&mse->mse_miscdevice);
    if (ret_val != 0) {
        pr_err("ERROR: No se pudo registrar el dispositivo %s\n", mse->mse_miscdevice.name);
        return ret_val;
    }

    pr_info("INFO: Dispositivo %s: minor asignado: %i\n", mse->mse_miscdevice.name, mse->mse_miscdevice.minor);

    return 0;
}

static int mpu9250_remove(struct i2c_client * client)  {

    struct mpu9250_dev * mse; /* Puntero a la estructura privada del device */

    /* Get device structure from bus device context */
    mse = i2c_get_clientdata(client);

    /* Deregister misc device */
    misc_deregister(&mse->mse_miscdevice);

    pr_info("INFO: Dispoisitvo removido: %s\n", mse->mse_miscdevice.name);

    return 0;
}

/*--------------------------------------------------------------------------------*/

static struct i2c_driver mpu9250_driver = {
    .probe= mpu9250_probe,
    .remove= mpu9250_remove,
    .driver = {
        .name = "mpu9250_driver",
        .owner = THIS_MODULE,
        .of_match_table = of_match_ptr(mpu9250_dt_ids),
    },
};

/*----------------------------------------------------------------------*/



/**********************************************************************
 * Esta seccion define cuales funciones seran las ejecutadas al cargar o
 * remover el modulo respectivamente. Es hecho implicitamente,
 * termina declarando init() exit()
 **********************************************************************/
module_i2c_driver(mpu9250_driver);

/**********************************************************************
 * Seccion sobre Informacion del modulo
 **********************************************************************/
MODULE_AUTHOR("Matias A. Paramidani <matiasparamidani@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Driver para el multichip MPU9250, desarrollado para la cursada de IMD de la MSE");
MODULE_INFO(mse_imd, "El objetivo de este trabajo es meramente academico.");
MODULE_VERSION("1.0");
