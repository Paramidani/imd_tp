# imd_tp

Repositorio para el proyecto final de la materia Implementación de Manejadores de Dispositivos de la MSE.

## Modulo MPU9250

El MPU-9250 es un módulo multi-chip (MCM) que consiste en dos pequeñas obleas de silicio integradas en un único encapsulado QFN. Uno de ellos contiene un giróscopo de 3 ejes y un acelerómetro de 3 ejes. El otro contiene un magnetómetro AK8963 de 3 ejes de Asahi Kasei Microdevices Corporation.

Además de otras funcionalidades, cuenta con un procesador y sensor de temperatura todo integrado en un encapsulado QFN de 3x3x1mm.

Tiene tanto una interfaz I2C como SPI para comunicar sus datos.

- [Hoja de datos](https://invensense.tdk.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf)
- [Mapa de memoria](https://invensense.tdk.com/wp-content/uploads/2015/02/RM-MPU-9250A-00-v1.6.pdf)

### Diagrama en bloques
![Diagrama en bloques del MPU9250](./imagenes/block_diagram.jpg)

El MPU9250 esta comprendido por los siguiente bloques y funciones principales:

- Giroscopio de 3 ejes (MEMs) con un ADC de 16 bits asociado
- Acelerómetro de 3 ejes (MEMs) con un ADC de 16 bits asociado
- Magnetómetro de 3 ejes (MEMs) con un ADC de 16 bits asociado
- Sensor de temperatura
- Procesador digital interno (DMP)
- Interfaces de comunicación I2C y SPI
- FIFO
- Interrupciones

### Operaciones de lectura y escritura por I2C

![Operaciones de escritura](./imagenes/write_operations.jpg)

![Operaciones de lectura](./imagenes/read_operations.jpg)

![Terminos](./imagenes/operations_terms.jpg)

### Conexion del modulo MPU9250 a la BeagleBone Black

![MPU9250 Board](./imagenes/mpu9250_board.png)

![Beagle Bone Black Pinout](./imagenes/beaglebone_black_pinmap.png)

![Conexionado](./imagenes/conexionado.png)

## Driver
[link](https://gitlab.com/Paramidani/imd_tp/-/blob/main/driver/README.md)

## Espacio de usuario
[link](https://gitlab.com/Paramidani/imd_tp/-/blob/main/user_space/README.md)

## Instrucciones de booteo

1. Enchufar el adaptador serie a un puerto USB.
2. Abrir una terminal y correr el siguiente comando: "picocom -b 115200 -r -l /dev/ttyUSB0"
3. Mientras se mantiene presionado el boton S2 de la beaglebone black, enchufar el cable USB de alimentacion y presionar una tecla cualquiera en la terminal para detener la secuencia de autobooteo.
4. Ejecutar los siguiente comandos en la consola de u-boot:

```
setenv autoload no
dhcp
ping 192.168.0.82
setenv serverip 192.168.0.82
tftp 0x81000000 zImage
tftp 0x82000000 am335x-boneblack.dtb
setenv bootargs root=/dev/nfs rw ip=dhcp console=ttyS0,115200n8 nfsroot=192.168.0.82:/home/mparamidani/MSE/ISO_II/nfsroot,nfsvers=3
bootz 0x81000000 - 0x82000000
```

5. Una vez que termine la secuencia de booteo y el kernel se cargue hay que loggearse con usuario "root" y sin password.

## Pruebas realizadas

### Pruebas simple de escritura (write_test.c)

Esta prueba se realizó para verificar el funcionamiento de la operación write(). Para eso se eligió el registro de configuración 0x6B donde se escribió el dato 0x01. Mediante un analizador lógico se verificaron las formas de onda de las señales SDA y SCL. También se comprobó que la dirección de dispositivo enviada fuera la correcta.

```c
int main(void)
{
    int ret_wr = 0;
    char msg[2];
    char reg_address;
    char data;

    reg_address = 0x6B;
    data = 0x01;

    msg[0] = reg_address;
    msg[1] = data;

    int my_dev = open("/dev/mse00", O_RDWR);
    if (my_dev < 0) {
        perror("ERROR: Fail to open device file: /dev/mse00.\n");
    } else {
        printf("INFO: Device open succesfully.\n");
        ret_wr = write(my_dev, msg, sizeof(msg));
        if (ret_wr < 0) {
            printf("Failed to write the message to the device.\n");
            return errno;
        }
        printf("INFO: Closing device.\n");
        close(my_dev);
    }
    return 0;
}
```

### Prueba simple de lectura (read_test.c)

Esta prueba se realizó para verificar el funcionamiento de la operación write(). Para eso se eligió el registro de configuración 0x6B donde se leyó el dato escrito por el test anterior. Mediante un analizador lógico se verificaron las formas de onda de las señales SDA y SCL. También se comprobó que la dirección de dispositivo enviada fuera la correcta.

```c
int main(void)
{
    int ret_wr = 0;
    int ret_rd = 0;
    char msg[2];
    char reg_address;

    reg_address = 0x6B;

    int my_dev = open("/dev/mse00", O_RDWR);
    if (my_dev < 0) {
        perror("ERROR: Fail to open device file: /dev/mse00.\n");
    } else {
        printf("INFO: Device open succesfully.\n");
        ret_wr = write(my_dev, &reg_address, sizeof(reg_address));
        if (ret_wr < 0) {
            printf("ERROR: Failed to write the message to the device.\n");
            return errno;
        }
        ret_rd = read(my_dev, &msg, sizeof(msg));
        if (ret_rd < 0) {
            printf("ERROR: Failed to read a message from the device.\n");
            return errno;
        }
        else {
            printf("INFO: Value read is: %x - %x\n", msg[1], msg[0]);
        }
        printf("INFO: Closing device.\n");
        close(my_dev);
    }
    return 0;
}
```

### Prueba de aplicación

El objetivo de esta pruba fue verificar el correcto funcionamiento de la biblioteca creada para las funciones de espacio de usuario: *imd_mpu9250.c* e *imd_mpu9250.h*. Esta se creo utilizando como base la biblioteca de la SAPI (firmware_v3), gran parte del código fue reutilizado. La excepción fueron algunas funciones como, por ejemplo, mpu9250WriteRegister() y mpu9250ReadRegister().

El programa test_imd_mpu9250.c primero hace uso de la función de configuración mpu9250Init() que setea las configuraciones necesarias para poder realizar las mediciones en cada sensor. Luego realiza 30 lecturas a cada sensor espaciadas por (aprox) 1 segundo y muestra los resultados ya escalados en pantalla. 

```c
#define MEAS_CYCLES 30

int main(){
    int i = 0;
    printf("INFO: Inicia la prueba del driver para el dispositivo MPU9250.\n");

    mpu9250Init( MPU9250_ADDRESS_0 );
    while(i < MEAS_CYCLES){
        mpu9250Read();

        printf( "Giróscopo:      (%f, %f, %f)   [rad/s]\r\n",
                mpu9250GetGyroX_rads(),
                mpu9250GetGyroY_rads(),
                mpu9250GetGyroZ_rads()
                );
        printf( "Acelerómetro:   (%f, %f, %f)   [m/s2]\r\n",
                mpu9250GetAccelX_mss(),
                mpu9250GetAccelY_mss(),
                mpu9250GetAccelZ_mss()
                );
		printf( "Magnetometro:   (%f, %f, %f)   [uT]\r\n",
                mpu9250GetMagX_uT(),
                mpu9250GetMagY_uT(),
                mpu9250GetMagZ_uT()
                );
        printf( "Temperatura:    %f   [C]\r\n\r\n",
                mpu9250GetTemperature_C()
                );
        usleep(1000000);
        i++;
    }
    printf("INFO: Fin del programa de prueba\n");
    return 0;
}
```

A continuación se pueden observar las formas de onda de una operación de lectura en ráfaga a los registros de todos los sensores, en total son 21 lecturas a partir de la dirección 0x3B.

![Operación de lectura en ráfaga](./imagenes/read_completo.jpg)

![Contenido de cada byte](./imagenes/read_completo_bytes.jpg)

![Detalle de las operaciones de escritura](./imagenes/write_devAddr_and_regAddr.jpg)

![Detalle de las operaciones de lectura](./imagenes/read_detail.jpg)